# README

This project's purpose is to create a searchable database
of recipes

Recipes should be searchable by ingredient, type of meal
(i.e. breakfast, lunch, etc...), time to prepare (i.e. quick
meals vs longer meals)
