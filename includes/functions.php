<?php
	require 'config.php';

/*
|--------------------------------------------------------
|             Database Connection function               |
|--------------------------------------------------------	
*/
function connect($config) {
	try {
		$conn = new PDO('mysql:host=localhost;dbname=recipes',
						$config['DB_USERNAME'],
						$config['DB_PASSWORD']);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $conn;
	} catch (PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
		return false;
	}
}

/*
|--------------------------------------------------------
|             Create Record function                     |
|--------------------------------------------------------
*/



/*
|--------------------------------------------------------
|             Return Record function                     |
|--------------------------------------------------------
*/
function returnRecord($table_name, $id) {
	#$id = 1;	// temporary
	$sqlText = 'SELECT * FROM $table_name WHERE id = :id';
	$stmt = $conn->prepare($sqlText);
	$stmt->setFetchMode(PDO::FETCH_OBJ);	// sets all queries to object mode
	#$stmt->setFetchMode(PDO::FETCH_ASSOC);	// sets all queries to associative array mode
	$stmt->execute(array(
					'id' => $id
					));
	while ($row = $stmt->fetch()) {
		print_r($row);
	}

	// alternate method
	#$stmt->bindParam(':id', $id, PDO::PARAM_INT);	// string would be PARAM_STR
	#$stmt->execute();

	#while ($row = $stmt->fetch()) {
	#	print_r($row);
	#}

	// an alternate while loop, returns results as objects; the default method returns an array
	#while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
	#	print_r($row);
	#}
}




/*
|--------------------------------------------------------
|             Return All Records function                |
|--------------------------------------------------------
*/
function returnAllRecords($columns, $table_name, $conn) {
	try {
		$result = $conn->query("SELECT $columns FROM $table_name");
		return ($result->rowCount() > 0) ? $result : false;
	} catch (Exception $e) {
		return false;
	}
}





/*
|---------------------------------------------------------
|             Return Records by Keyword function          |
|---------------------------------------------------------
*/
function returnRecordsByKeyword($table_name, $meal_type, $conn) {
	$result = $conn->query("SELECT $");
}





 */
/*
|---------------------------------------------------------
|             Update Record function                      |
|---------------------------------------------------------
*/









/*
|----------------------------------------------------------
|             Delete Record function                       |
|----------------------------------------------------------
*/