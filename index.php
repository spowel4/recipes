

<!doctype html>

<html lang="en" class="no-js">
	<head>
		<!-- index.php -->
		<meta charset="utf-8" />
		<title>Recipe Database</title>

		<!-- css -->
		<link rel="stylesheet" href="css/reset.css">
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/style.css">
		<!-- end css -->
		<!-- Always place modernizer after your stylesheets -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
	</head>

	<body>
		<div id="container">
			<h1>Recipe Database</h1>
			<form name="mainForm" id="" method="post" action="views/recipesByCategory.php">
				<label for="searchString">Keyword Search</label>
				<input type="text" id="searchString" size="50" />
				<input type="submit" value="Search Recipes" />
				<label for="mealType">Recipe Categories</label>
				<select name = 'mealType' id='mealType'>
					<option value="NoSelection">Select Category</option>
					<?php
						if ($categories) {
							foreach ($categories as $category) {
								echo "<option value='" . $category['mealType'] . "'>" . $category['mealType'] . "</option>";
							}
						};
					?>
				</select>
			</form>
		</div><!-- end of container div -->
		<script src="js/vendor/jquery-1.9.0.min.js"></script>

		<!-- js -->
		<script src="../js/script.js">
		<!-- end js -->
	</body>
</html>