<?php
	require '../includes/functions.php';
	$conn = connect($config);
	$table_name = 'recipe';
	$meal_type = $_POST['mealType'];
	$search_string = $_POST['searchString'];
	echo $table_name . "<br>";
	echo $meal_type . "<br>";
	if ($conn) {
		$recipes = returnRecordsByKeyword($table_name, $meal_type, $conn);
	} else {
		echo 'Could not connect to the database';
	};
?>

<!doctype html>

<html lang="en" class="no-js">
	<head>
		<!-- recipesByCategry.php -->
		<meta charset="utf-8">
		<title>Recipes by Category</title>

		<!-- css -->
		<link rel="stylesheet" href="../css/reset.css">
		<link rel="stylesheet" href="../css/normalize.css">
		<link rel="stylesheet" href="../css/style.css">
		<!-- end css -->
		<!-- Always place modernizer after your stylesheets -->
		<script src="../js/vendor/modernizr-2.6.2.min.js"></script>
	</head>

	<body>
		<div id="container">
			<h1>Recipes by Category</h1>
			<?php
				if ($recipes) {
					foreach ($recipes as $recipe) {
						echo $recipe['title'] . "<br>";
					}
				} else {
					echo 'no recipes';
				}
			?>
		</div><!-- end of container div -->
		<script src="../js/vendor/jquery-1.9.0.min.js"></script>

		<!-- js -->
		<script src="../js/script.js"></script>
		<!-- end js -->
	</body>
</html>