<?php
	require '../includes/functions.php';

?>

<!doctype html>

<html lang="en">
	<head>
		<!-- searchResults.php -->
		<meta charset="utf-8" />
		<title>Search Results</title>

		<!-- css -->
		<link rel="stylesheet" href="../css/reset.css">
		<link rel="stylesheet" href="../css/style.css">
		<!-- end css -->
	</head>

	<body>
		<h1>Search Results</h1>

		<script src="http://code.jquery.com/jquery-1.8.0.min.js">

		<!-- js -->
		<script src="../js/script.js">
		<!-- end js -->
	</body>
</html>